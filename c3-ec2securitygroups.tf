# Create Security Group - SSH Traffic
resource "aws_security_group" "vpc-ssh" {
  name        = "vpc-ssh"
  description = "Dev VPC SSH"
  ingress { #inbounds rules for security group
    description = "Allow Port 22"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress { #inbounds rules for security group
    description = "Allow Port 10250"
    from_port   = 10250
    to_port     = 10250
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress { #inbounds rules for security group
    description = "Allow Port 10259"
    from_port   = 10259
    to_port     = 10259
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress { #inbounds rules for security group
    description = "Allow Port 6443"
    from_port   = 6443
    to_port     = 6443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress { #inbounds rules for security group
    description = "Allow Port 2379 - 2380"
    from_port   = 2379 
    to_port     = 2379 
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
   ingress { #inbounds rules for security group
    description = "Allow Port 2379 - 2380"
    from_port   = 2380 
    to_port     = 2380 
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
    ingress { #inbounds rules for security group
    description = "Allow Port 10257"
    from_port   = 10257
    to_port     = 10257
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress { # outbounds rules for security group
    description = "Allow all ip and ports outbound"    
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "vpc-ssh"
  }
}



