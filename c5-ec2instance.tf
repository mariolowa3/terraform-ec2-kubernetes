# EC2 Instance
resource "aws_instance" "myec2vm" {
  ami = data.aws_ami.amzubuntu.id
  instance_type = var.instance_type
  key_name = var.instance_keypair
  vpc_security_group_ids = [ aws_security_group.vpc-ssh.id ]
  tags = {
    "Name" = "EC2 master node"
  }
}
